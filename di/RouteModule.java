package ru.trmedia.trbtlservice.di.modules;

import android.support.annotation.NonNull;

import com.pushtorefresh.storio3.sqlite.StorIOSQLite;

import dagger.Module;
import dagger.Provides;
import ru.trmedia.trbtlservice.common.PreferencesApi;
import ru.trmedia.trbtlservice.data.dbsources.DbSource;
import ru.trmedia.trbtlservice.data.dbsources.store.StoreDbSource;
import ru.trmedia.trbtlservice.data.dbsources.visit.VisitDbSource;
import ru.trmedia.trbtlservice.data.mappers.StoreMapper;
import ru.trmedia.trbtlservice.data.mappers.VisitMapper;
import ru.trmedia.trbtlservice.data.models.stores.StoreDb;
import ru.trmedia.trbtlservice.data.models.visit.VisitDb;
import ru.trmedia.trbtlservice.data.network.TRBTLRetrofitInterface;
import ru.trmedia.trbtlservice.data.network.models.StoreNetwork;
import ru.trmedia.trbtlservice.data.network.models.VisitNetwork;
import ru.trmedia.trbtlservice.data.network.sources.NetworkSource;
import ru.trmedia.trbtlservice.data.network.sources.StoreNetworkSource;
import ru.trmedia.trbtlservice.data.network.sources.VisitNetworkSource;
import ru.trmedia.trbtlservice.data.repository.StoreRepositoryImpl;
import ru.trmedia.trbtlservice.data.repository.VisitRepositoryImpl;
import ru.trmedia.trbtlservice.di.scopes.RouteScreen;
import ru.trmedia.trbtlservice.domain.interactors.StoresByDateInteractorImpl;
import ru.trmedia.trbtlservice.domain.interactors.VisitByDateInteractorImpl;
import ru.trmedia.trbtlservice.domain.interfaces.StoresByDateInteractor;
import ru.trmedia.trbtlservice.domain.interfaces.VisitByDateInteractor;
import ru.trmedia.trbtlservice.domain.repositories.StoreRepository;
import ru.trmedia.trbtlservice.domain.repositories.VisitRepository;

@Module
public class RouteModule {
    @NonNull
    @RouteScreen
    @Provides
    DbSource<StoreDb> provideStoreDbSource(@NonNull StorIOSQLite storIOSQLite) {
        return new StoreDbSource(storIOSQLite);
    }

    @NonNull
    @RouteScreen
    @Provides
    DbSource<VisitDb> provideVisitDbSource(@NonNull StorIOSQLite storIOSQLite) {
        return new VisitDbSource(storIOSQLite);
    }


    @NonNull
    @RouteScreen
    @Provides
    StoresByDateInteractor provideRouteByDateInteractor(
            @NonNull StoreRepositoryImpl storeRepositoryImpl) {
        return new StoresByDateInteractorImpl(storeRepositoryImpl);
    }

    @NonNull
    @RouteScreen
    @Provides
    VisitByDateInteractor provideVisitByDateInteractor(
            @NonNull VisitRepository routeRepository) {
        return new VisitByDateInteractorImpl(routeRepository);
    }

    @NonNull
    @RouteScreen
    @Provides
    StoreRepository provideRouteRepository(
            @NonNull DbSource<StoreDb> storeDbSource,
            @NonNull StoreMapper storeMapper,
            @NonNull StoreNetworkSource storeNetworkSource) {
        return new StoreRepositoryImpl(storeDbSource, storeNetworkSource, storeMapper);
    }

    @NonNull
    @RouteScreen
    @Provides
    StoreMapper provideStoreMapper() {
        return new StoreMapper();
    }

    @NonNull
    @RouteScreen
    @Provides
    VisitMapper provideVisitMapper() {
        return new VisitMapper();
    }

    @NonNull
    @RouteScreen
    @Provides
    VisitRepository provideVisitRepository(
            @NonNull DbSource<VisitDb> visitDbSource,
            @NonNull VisitMapper visitMapper,
            @NonNull NetworkSource<VisitNetwork> visitNetworkSource) {
        return new VisitRepositoryImpl(visitDbSource, visitNetworkSource, visitMapper);
    }

    @NonNull
    @RouteScreen
    @Provides
    NetworkSource<StoreNetwork> provideStoreNetworkSource(
            @NonNull TRBTLRetrofitInterface trbtlRetrofitInterface,
            @NonNull PreferencesApi preferencesApi) {
        return new StoreNetworkSource(trbtlRetrofitInterface, preferencesApi);
    }

    @NonNull
    @RouteScreen
    @Provides
    NetworkSource<VisitNetwork> provideVisitNetworkSource(
            @NonNull TRBTLRetrofitInterface trbtlRetrofitInterface,
            @NonNull PreferencesApi preferencesApi) {
        return new VisitNetworkSource(trbtlRetrofitInterface, preferencesApi);
    }
}
