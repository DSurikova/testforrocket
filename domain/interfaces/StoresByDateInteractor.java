package ru.trmedia.trbtlservice.domain.interfaces;

import org.joda.time.LocalDate;

import java.util.List;

import io.reactivex.Single;
import ru.trmedia.trbtlservice.domain.models.Store;

public interface StoresByDateInteractor {
    Single<List<Store>> getStoresByDate(LocalDate localDate);
}
