package ru.trmedia.trbtlservice.domain.repositories;

import org.joda.time.LocalDate;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;
import ru.trmedia.trbtlservice.common.Optional;
import ru.trmedia.trbtlservice.domain.models.Store;
import ru.trmedia.trbtlservice.domain.models.Visit;

public interface VisitRepository {
    Single<Optional<Visit>> getVisitByStoreAndDate(Store store, LocalDate localDate);

    Completable saveVisit(Visit visit, boolean shouldSendToServer);

    Completable removeVisit(Visit visit);

    Single<List<Visit>> getNotSentVisits();
}
