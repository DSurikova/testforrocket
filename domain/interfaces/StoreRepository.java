package ru.trmedia.trbtlservice.domain.repositories;

import org.joda.time.LocalDate;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;
import ru.trmedia.trbtlservice.domain.models.Store;

public interface StoreRepository {
    Single<List<Store>> getStoresByDate(LocalDate localDate);

    Single<List<Store>> getAllStoresDb();

    Single<List<Store>> getAllStoresNetwork();

    Completable removeAllStores();
}
