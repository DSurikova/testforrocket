package ru.trmedia.trbtlservice.domain.interfaces;

import org.joda.time.LocalDate;

import io.reactivex.Single;
import ru.trmedia.trbtlservice.common.Optional;
import ru.trmedia.trbtlservice.domain.models.Store;
import ru.trmedia.trbtlservice.domain.models.Visit;

public interface VisitByDateInteractor {
    Single<Optional<Visit>> getVisitByStoreAndDate(Store store, LocalDate localDate);
}
