package ru.trmedia.trbtlservice.domain.interactors;

import org.joda.time.LocalDate;

import io.reactivex.Single;
import ru.trmedia.trbtlservice.common.Optional;
import ru.trmedia.trbtlservice.domain.interfaces.VisitByDateInteractor;
import ru.trmedia.trbtlservice.domain.models.Store;
import ru.trmedia.trbtlservice.domain.models.Visit;
import ru.trmedia.trbtlservice.domain.repositories.VisitRepository;

public class VisitByDateInteractorImpl implements VisitByDateInteractor {
    private final VisitRepository visitRepository;

    public VisitByDateInteractorImpl(VisitRepository visitRepository){
        this.visitRepository = visitRepository;
    }

    public Single<Optional<Visit>> getVisitByStoreAndDate(Store store, LocalDate localDate){
        return visitRepository.getVisitByStoreAndDate(store, localDate);
    }
}
