package ru.trmedia.trbtlservice.domain.interactors;

import android.support.annotation.NonNull;

import org.joda.time.LocalDate;

import java.util.List;

import io.reactivex.Single;
import ru.trmedia.trbtlservice.domain.interfaces.StoresByDateInteractor;
import ru.trmedia.trbtlservice.domain.models.Store;
import ru.trmedia.trbtlservice.domain.repositories.StoreRepository;

public class StoresByDateInteractorImpl implements StoresByDateInteractor {

    private final StoreRepository storeRepository;


    public StoresByDateInteractorImpl(StoreRepository storeRepository){
        this.storeRepository = storeRepository;
    }

    @NonNull
    @Override
    public Single<List<Store>> getStoresByDate(LocalDate localDate) {
        return storeRepository.getStoresByDate(localDate);
    }
}
