package ru.trmedia.trbtlservice.domain.models;

public class Store {
    protected int id;
    protected String code;
    protected String storeName;
    protected String externalStoreName;
    protected String segment;
    protected String address;
    protected double latitude;
    protected double longitude;
    protected String channelCode;
    protected String channelName;
    protected String customerName;
    protected String customerCode;


    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getId() {
        return id;
    }

    public String getCode() {
        return code;
    }

    public String getCustomerName() {
        return customerName;
    }

    public String getChannelName() {
        return channelName;
    }

    public String getSegment() {
        return segment;
    }

    public String getStoreAddress() {
        return address;
    }

    public double getLongitude() {
        return longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }


    public String getChannelCode() {
        return channelCode;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getExternalStoreName() {
        return externalStoreName;
    }

    public void setExternalStoreName(String externalStoreName) {
        this.externalStoreName = externalStoreName;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setSegment(String segment) {
        this.segment = segment;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }
}
