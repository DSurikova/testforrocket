package ru.trmedia.trbtlservice.presentation.presenters;

import android.support.annotation.NonNull;

import com.arellomobile.mvp.InjectViewState;

import org.joda.time.LocalDate;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import ru.trmedia.trbtlservice.TRBTLApp;
import ru.trmedia.trbtlservice.domain.interfaces.StoresByDateInteractor;
import ru.trmedia.trbtlservice.domain.interfaces.VisitByDateInteractor;
import ru.trmedia.trbtlservice.domain.models.Store;
import ru.trmedia.trbtlservice.presentation.mappers.StoreVisitMapper;
import ru.trmedia.trbtlservice.presentation.models.StoreVisitUI;
import ru.trmedia.trbtlservice.presentation.views.RoutesByDateView;


@InjectViewState
public class RoutesByDatesPresenter extends BasePresenter<RoutesByDateView> {

    @Inject
    StoresByDateInteractor storesByDateInteractor;
    @Inject
    VisitByDateInteractor visitByDateInteractor;
    @Inject
    StoreVisitMapper storeVisitMapper;

    private final CompositeDisposable compositeDisposable;

    public RoutesByDatesPresenter() {
        TRBTLApp.getInstance().plusRouteComponent().inject(this);
        compositeDisposable = new CompositeDisposable();
    }

    public void getStores(LocalDate localDate) {
        Disposable d = getStoreWithVisits(localDate)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable ->   getViewState().onShowProgressLoad())
                .subscribe(this::success, this::error);
        compositeDisposable.add(d);
    }

    private Single<List<StoreVisitUI>> getStoreWithVisits(LocalDate localDate) {
        return storesByDateInteractor.getStoresByDate(localDate)
                .flattenAsObservable(stores -> stores)
                .flatMapSingle(store -> getStoreWithVisit(store, localDate))
                .toList();
    }

    private Single<StoreVisitUI> getStoreWithVisit(Store store, LocalDate localDate){
        return visitByDateInteractor.getVisitByStoreAndDate(store, localDate)
                .map(optional -> storeVisitMapper.apply(store, optional));
    }

    private void success(@NonNull List<StoreVisitUI> visitByDates) {
        getViewState().onHideProgressLoad();
        getViewState().onShowStores(visitByDates);
    }

    private void error(@NonNull Throwable throwable) {
        getViewState().onHideProgressLoad();
        getViewState().onError(throwable);
    }

    public void stop() {
        TRBTLApp.getInstance().clearRouteComponent();
        compositeDisposable.clear();
    }


}
