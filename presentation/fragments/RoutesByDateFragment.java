package ru.trmobile.fragments;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.prolificinteractive.materialcalendarview.CalendarDay;

import org.joda.time.LocalDate;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.trmedia.trbtlservice.presentation.models.StoreVisitUI;
import ru.trmedia.trbtlservice.presentation.presenters.RoutesByDatesPresenter;
import ru.trmedia.trbtlservice.presentation.ui.adapters.RoutesRecyclerAdapter;
import ru.trmedia.trbtlservice.presentation.ui.custom_views.BTLCalendarView;
import ru.trmedia.trbtlservice.presentation.views.RoutesByDateView;
import ru.trmobile.R;
import ru.trmobile.dialogs.BTLDialogs;


public class RoutesByDateFragment extends TrBaseFragment implements RoutesByDateView {

    @InjectPresenter
    RoutesByDatesPresenter presenter;

    @BindView(R.id.cvDate)
    BTLCalendarView cvDate;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.llVisitPanelLoad)
    LinearLayout llVisitPanelLoad;
    RoutesRecyclerAdapter recyclerAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list_items_by_dates, container, false);
        ButterKnife.bind(this, view);
        initUI();
        return view;
    }

    public void initUI() {
        initCalendar();
    }

    private void initCalendar() {
        setCurrentDate();
        setOnDateSelected();
    }

    private void setOnDateSelected() {
        cvDate.setOnDateChangedListener((widget, date, selected) -> {
            LocalDate selectedDate = convertWidgetDateToJoda(date);
            presenter.getStores(selectedDate);
        });
    }

    private LocalDate convertWidgetDateToJoda(CalendarDay date) {
        return new LocalDate(date.getDate());
    }

    private void setCurrentDate() {
        LocalDate localDate = new LocalDate();
        cvDate.setSelectedDate(localDate.toDate());
    }

    public void initAdapter(List<StoreVisitUI> storeVisitUIS) {
        recyclerAdapter = new RoutesRecyclerAdapter(storeVisitUIS);
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(llm);
        recyclerView.setAdapter(recyclerAdapter);
    }

    @Override
    public void onError(Throwable e) {
        BTLDialogs.showOneButtonDialog(getActivity(), getString(R.string.ok),getString(R.string.error),getString(R.string.cant_get_routes));
    }

    @Override
    public void onShowStores(List<StoreVisitUI> storeVisitUIS) {
        showStores(storeVisitUIS);
    }

    private void showStores(List<StoreVisitUI> storeVisitUIS) {
        if (recyclerAdapter == null ){
            initAdapter(storeVisitUIS);
        }else{
            setAdapterData(storeVisitUIS);
        }
    }

    private void setAdapterData(List<StoreVisitUI> storeVisitUIS) {
        recyclerAdapter.setData(storeVisitUIS);
    }

    @Override
    public void onShowProgressLoad() {
        recyclerView.setVisibility(View.GONE);
        llVisitPanelLoad.setVisibility(View.VISIBLE);
    }

    @Override
    public void onHideProgressLoad() {
        recyclerView.setVisibility(View.VISIBLE);
        llVisitPanelLoad.setVisibility(View.GONE);
    }

    @Override
    public void onStop() {
        super.onStop();
        presenter.stop();
    }
}
