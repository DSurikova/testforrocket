package ru.trmedia.trbtlservice.presentation.models;

public class StoreVisitUI {
    protected String storeName;
    protected EnVisitStatus visitStatus;
    public enum  EnVisitStatus {
        SENT, ENDED, NO_ENDED_VISIT
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public EnVisitStatus getVisitStatus() {
        return visitStatus;
    }

    public void setVisitStatus(EnVisitStatus visitStatus) {
        this.visitStatus = visitStatus;
    }


}
