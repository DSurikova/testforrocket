package ru.trmedia.trbtlservice.data.dbsources;

import android.support.annotation.NonNull;

import com.pushtorefresh.storio3.sqlite.queries.DeleteQuery;
import com.pushtorefresh.storio3.sqlite.queries.RawQuery;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;
import ru.trmedia.trbtlservice.common.Optional;

public interface DbSource<T> {
    Completable addItem(@NonNull T item);

    Completable addItems(@NonNull List<T> items);

    Completable updateItem(@NonNull T item);

    Completable removeItem(@NonNull T item);

    Completable removeItems(@NonNull DeleteQuery deleteQuery);

    Single<List<T>> getItems(@NonNull RawQuery query);

    Single<Optional<T>> getItem(@NonNull  RawQuery query);
}
