package ru.trmedia.trbtlservice.data.dbsources.store;

import android.support.annotation.NonNull;

import com.pushtorefresh.storio3.sqlite.StorIOSQLite;
import com.pushtorefresh.storio3.sqlite.queries.DeleteQuery;
import com.pushtorefresh.storio3.sqlite.queries.RawQuery;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;
import ru.trmedia.trbtlservice.common.Optional;
import ru.trmedia.trbtlservice.data.dbsources.DbSource;
import ru.trmedia.trbtlservice.data.models.stores.StoreDb;
import ru.trmedia.trbtlservice.data.models.visit.VisitDb;

public class StoreDbSource implements DbSource<StoreDb> {
    @NonNull
    private final StorIOSQLite storIOSQLite;

    public StoreDbSource(@NonNull StorIOSQLite storIOSQLite){
        this.storIOSQLite = storIOSQLite;
    }

    @Override
    public Completable addItem(@NonNull StoreDb item) {
        return storIOSQLite
                .put()
                .object(item)
                .prepare()
                .asRxCompletable();
    }

    @Override
    public Completable addItems(@NonNull List<StoreDb> items) {
        return storIOSQLite
                .put()
                .objects(items)
                .prepare()
                .asRxCompletable();
    }

    @Override
    public Completable updateItem(@NonNull StoreDb item) {
        return storIOSQLite
                .put()
                .object(item)
                .prepare()
                .asRxCompletable();
    }

    @Override
    public Completable removeItem(@NonNull StoreDb item) {
        return storIOSQLite
                .delete()
                .object(item)
                .prepare()
                .asRxCompletable();
    }

    @Override
    public Completable removeItems(@NonNull DeleteQuery deleteQuery) {
        return storIOSQLite
                .delete()
                .byQuery(deleteQuery)
                .prepare()
                .asRxCompletable();
    }

    @Override
    public Single<List<StoreDb>> getItems(@NonNull RawQuery query) {
        return storIOSQLite
                .get()
                .listOfObjects(StoreDb.class)
                .withQuery(query)
                .prepare()
                .asRxSingle();
    }

    @Override
    public Single<Optional<StoreDb>> getItem(@NonNull RawQuery query) {
        return storIOSQLite
                .get()
                .object(VisitDb.class)
                .withQuery(query)
                .prepare()
                .asRxSingle()
                .map(visitOptional -> new Optional(visitOptional.orNull()));
    }
}
