package ru.trmedia.trbtlservice.data.dbsources.visit;

import android.support.annotation.NonNull;

import com.pushtorefresh.storio3.sqlite.StorIOSQLite;
import com.pushtorefresh.storio3.sqlite.queries.DeleteQuery;
import com.pushtorefresh.storio3.sqlite.queries.RawQuery;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;
import ru.trmedia.trbtlservice.common.Optional;
import ru.trmedia.trbtlservice.data.dbsources.DbSource;
import ru.trmedia.trbtlservice.data.models.visit.VisitDb;

public class VisitDbSource implements DbSource<VisitDb> {
    @NonNull
    private final StorIOSQLite storIOSQLite;

    public VisitDbSource(@NonNull StorIOSQLite storIOSQLite){
        this.storIOSQLite = storIOSQLite;
    }

    @Override
    public Completable addItem(@NonNull VisitDb item) {
        return storIOSQLite
                .put()
                .object(item)
                .prepare()
                .asRxCompletable();
    }

    @Override
    public Completable addItems(@NonNull List<VisitDb> items) {
        return storIOSQLite
                .put()
                .objects(items)
                .prepare()
                .asRxCompletable();
    }

    @Override
    public Completable updateItem(@NonNull VisitDb item) {
        return storIOSQLite
                .put()
                .object(item)
                .prepare()
                .asRxCompletable();
    }

    @Override
    public Completable removeItem(@NonNull VisitDb item) {
        return storIOSQLite
                .delete()
                .object(item)
                .prepare()
                .asRxCompletable();
    }

    @Override
    public Completable removeItems(@NonNull DeleteQuery deleteQuery) {
        return storIOSQLite
                .delete()
                .byQuery(deleteQuery)
                .prepare()
                .asRxCompletable();
    }

    @Override
    public Single<List<VisitDb>> getItems(@NonNull RawQuery query) {
        return storIOSQLite
                .get()
                .listOfObjects(VisitDb.class)
                .withQuery(query)
                .prepare()
                .asRxSingle();
    }

    @Override
    public Single<Optional<VisitDb>> getItem(@NonNull RawQuery query) {
        return storIOSQLite
                .get()
                .object(VisitDb.class)
                .withQuery(query)
                .prepare()
                .asRxSingle()
                .map(visitOptional -> new Optional(visitOptional.orNull()));
    }
}
