package ru.trmedia.trbtlservice.data.models.visit;

import com.pushtorefresh.storio3.sqlite.annotations.StorIOSQLiteColumn;
import com.pushtorefresh.storio3.sqlite.annotations.StorIOSQLiteType;

import ru.trmedia.trbtlservice.data.db.tables.results.VisitsTable;


@StorIOSQLiteType(table = VisitsTable.TABLE_NAME)
public class VisitDb {
    @StorIOSQLiteColumn(name = VisitsTable.MOBILE_ID, key = true)
    protected String mobileId;
    @StorIOSQLiteColumn(name = VisitsTable.DATE)
    protected int date;
    @StorIOSQLiteColumn(name = VisitsTable.ROUTE_ID)
    protected int routeId;
    @StorIOSQLiteColumn(name = VisitsTable.STORE_ID)
    protected int storeId;
    @StorIOSQLiteColumn(name = VisitsTable.TIME_SPENT)
    protected long timeSpent;
    @StorIOSQLiteColumn(name = VisitsTable.START_TIME_BY_DEVICE)
    protected long startTimeByDevice;
    @StorIOSQLiteColumn(name = VisitsTable.START_TIME_BY_GPS)
    protected long startTimeByGPS;
    @StorIOSQLiteColumn(name = VisitsTable.START_TIME_BY_SERVER)
    protected long startTimeByServer;
    @StorIOSQLiteColumn(name = VisitsTable.SHOULD_BE_SENT)
    protected boolean shouldBeSent;
    @StorIOSQLiteColumn(name = VisitsTable.IS_REGISTERED)
    protected boolean isRegistered;
    @StorIOSQLiteColumn(name = VisitsTable.STATUS_TYPE_ID)
    protected int statusTypeId;
    @StorIOSQLiteColumn(name = VisitsTable.CANCEL_VISIT_REASON_ID)
    protected int cancelVisitReasonId;

    public VisitDb() {
    }

    public boolean isRegistered() {
        return isRegistered;
    }

    public long getTimeSpent() {
        return timeSpent;
    }

    public int getStoreId() {
        return storeId;
    }

    public int getRouteId() {
        return routeId;
    }

    public boolean isShouldBeSent() {
        return shouldBeSent;
    }

    public String getMobileId() {
        return mobileId;
    }

    public int getStatusTypeId() {
        return statusTypeId;
    }

    public void setCancelVisitReasonId(int cancelVisitReasonId) {
        this.cancelVisitReasonId = cancelVisitReasonId;
    }

    public int getDate() {
        return date;
    }

    public void setShouldBeSent(boolean shouldBeSent) {
        this.shouldBeSent = shouldBeSent;
    }

    public void setStatusTypeId(int statusTypeId) {
        this.statusTypeId = statusTypeId;
    }


    public long getStartTimeByDevice() {
        return startTimeByDevice;
    }

    public long getStartTimeByGPS() {
        return startTimeByGPS;
    }

    public long getStartTimeByServer() {
        return startTimeByServer;
    }


    public void setRegistered(boolean registered) {
        isRegistered = registered;
    }

    public void setTimeSpent(long timeSpent) {
        this.timeSpent = timeSpent;
    }

    public void setMobileId(String mobileId) {
        this.mobileId = mobileId;
    }

    public void setDate(int date) {
        this.date = date;
    }

    public void setRouteId(int routeId) {
        this.routeId = routeId;
    }

    public void setStoreId(int storeId) {
        this.storeId = storeId;
    }

    public void setStartTimeByDevice(long startTimeByDevice) {
        this.startTimeByDevice = startTimeByDevice;
    }

    public void setStartTimeByGPS(long startTimeByGPS) {
        this.startTimeByGPS = startTimeByGPS;
    }

    public void setStartTimeByServer(long startTimeByServer) {
        this.startTimeByServer = startTimeByServer;
    }

}
