package ru.trmedia.trbtlservice.data.models.stores;

import com.pushtorefresh.storio3.sqlite.annotations.StorIOSQLiteColumn;
import com.pushtorefresh.storio3.sqlite.annotations.StorIOSQLiteType;

import ru.trmedia.trbtlservice.data.db.tables.servers.StoresTable;

@StorIOSQLiteType(table = StoresTable.TABLE_NAME)
public class StoreDb {
    @StorIOSQLiteColumn(name = StoresTable.ID, key = true)
    protected int id;
    @StorIOSQLiteColumn(name = StoresTable.CODE)
    protected String code;
    @StorIOSQLiteColumn(name = StoresTable.STORE_NAME)
    protected String storeName;
    @StorIOSQLiteColumn(name = StoresTable.EXTERNAL_STORE_NAME)
    protected String externalStoreName;
    @StorIOSQLiteColumn(name = StoresTable.SEGMENT)
    protected String segment;
    @StorIOSQLiteColumn(name = StoresTable.PROMO_PERIOD)
    protected int promoperiod;
    @StorIOSQLiteColumn(name = StoresTable.STORE_ADDRESS)
    protected String address;
    @StorIOSQLiteColumn(name = StoresTable.LATITUDE)
    protected double latitude;
    @StorIOSQLiteColumn(name = StoresTable.LONGITUDE)
    protected double longitude;
    @StorIOSQLiteColumn(name = StoresTable.CHANNEL_CODE)
    protected String channelCode;
    @StorIOSQLiteColumn(name = StoresTable.CHANNEL_NAME)
    protected String channelName;
    @StorIOSQLiteColumn(name = StoresTable.CUSTOMER_NAME)
    protected String customerName;
    @StorIOSQLiteColumn(name = StoresTable.CUSTOMER_CODE)
    protected String customerCode;

    public StoreDb() {
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getId() {
        return id;
    }

    public String getCode() {
        return code;
    }

    public String getCustomerName() {
        return customerName;
    }

    public String getChannelName() {
        return channelName;
    }

    public String getSegment() {
        return segment;
    }

    public String getStoreAddress() {
        return address;
    }

    public double getLongitude() {
        return longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }


    public String getChannelCode() {
        return channelCode;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getExternalStoreName() {
        return externalStoreName;
    }

    public void setExternalStoreName(String externalStoreName) {
        this.externalStoreName = externalStoreName;
    }


}
