package ru.trmedia.trbtlservice.data.repository;

import android.support.annotation.NonNull;

import com.pushtorefresh.storio3.sqlite.queries.DeleteQuery;
import com.pushtorefresh.storio3.sqlite.queries.RawQuery;

import org.joda.time.LocalDate;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Single;
import ru.trmedia.trbtlservice.common.utils.DateUtils;
import ru.trmedia.trbtlservice.data.db.tables.servers.StoresTable;
import ru.trmedia.trbtlservice.data.dbsources.DbSource;
import ru.trmedia.trbtlservice.data.dbsources.store.specifications.StoreSpecification;
import ru.trmedia.trbtlservice.data.mappers.StoreMapper;
import ru.trmedia.trbtlservice.data.models.stores.StoreDb;
import ru.trmedia.trbtlservice.data.network.models.StoreNetwork;
import ru.trmedia.trbtlservice.data.network.sources.NetworkSource;
import ru.trmedia.trbtlservice.domain.models.Store;
import ru.trmedia.trbtlservice.domain.repositories.StoreRepository;

public class StoreRepositoryImpl implements StoreRepository {
    @NonNull
    private final DbSource<StoreDb> storeDbSource;
    @NonNull
    private final NetworkSource<StoreNetwork> storeNetworkSource;
    @NonNull
    private final StoreMapper storeMapper;

    @Inject
    public StoreRepositoryImpl(
            @NonNull DbSource<StoreDb> storeDbSource, @NonNull NetworkSource<StoreNetwork> storeNetworkSource, StoreMapper storeMapper){
        this.storeDbSource = storeDbSource;
        this.storeNetworkSource = storeNetworkSource;
        this.storeMapper = storeMapper;
    }

    public Single<List<Store>> getStoresByDate(LocalDate localDate) {
        String serverDate = DateUtils.getDateInServerFormatStr(localDate);
        StoreSpecification specification = new StoreSpecification();
        RawQuery rawQuery = RawQuery.builder()
                .query(specification.getStoresByDate(serverDate).toSqlQuery())
                .build();
        return storeDbSource.getItems(rawQuery)
                .flattenAsObservable(storeDbs -> storeDbs)
                .map(storeDb -> storeMapper.apply(storeDb))
                .toList();
    }

    @Override
    public Single<List<Store>> getAllStoresDb() {
        StoreSpecification specification = new StoreSpecification();
        RawQuery rawQuery = RawQuery.builder()
                .query(specification.getAll().toSqlQuery())
                .build();
        return storeDbSource.getItems(rawQuery)
                .flattenAsObservable(storeDbs -> storeDbs)
                .map(storeDb -> storeMapper.apply(storeDb))
                .toList();
    }

    @Override
    public Single<List<Store>> getAllStoresNetwork() {
        return storeNetworkSource.getItems()
                .flattenAsObservable(storesNetwork -> storesNetwork)
                .map(storeDb -> storeMapper.applyNetwork(storeDb))
                .toList()
                .flatMapCompletable(storeDbs -> storeDbSource.addItems(storeDbs))
                .andThen(getAllStoresDb());
    }

    @Override
    public Completable removeAllStores() {
        DeleteQuery deleteQuery = DeleteQuery.builder()
                .table(StoresTable.TABLE_NAME)
                .build();
        return storeDbSource.removeItems(deleteQuery);
    }


}
