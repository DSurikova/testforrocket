package ru.trmedia.trbtlservice.data.repository;

import android.support.annotation.NonNull;

import com.pushtorefresh.storio3.sqlite.queries.RawQuery;

import org.joda.time.LocalDate;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;
import ru.trmedia.trbtlservice.common.Optional;
import ru.trmedia.trbtlservice.common.utils.DateUtils;
import ru.trmedia.trbtlservice.data.dbsources.DbSource;
import ru.trmedia.trbtlservice.data.dbsources.visit.specificatons.VisitSpecification;
import ru.trmedia.trbtlservice.data.mappers.VisitMapper;
import ru.trmedia.trbtlservice.data.models.visit.VisitDb;
import ru.trmedia.trbtlservice.data.network.models.VisitNetwork;
import ru.trmedia.trbtlservice.data.network.sources.NetworkSource;
import ru.trmedia.trbtlservice.domain.models.Store;
import ru.trmedia.trbtlservice.domain.models.Visit;
import ru.trmedia.trbtlservice.domain.repositories.VisitRepository;

public class VisitRepositoryImpl implements VisitRepository {
    @NonNull
    private final DbSource<VisitDb> visitDbSource;
    @NonNull
    private final NetworkSource<VisitNetwork> visitNetworkSource;
    @NonNull
    private final VisitMapper visitMapper;

    public VisitRepositoryImpl(
            @NonNull DbSource<VisitDb> visitDbSource, @NonNull NetworkSource<VisitNetwork> visitNetworkSource,
            VisitMapper visitMapper) {
        this.visitDbSource = visitDbSource;
        this.visitNetworkSource = visitNetworkSource;
        this.visitMapper = visitMapper;
    }

    @Override
    public Single<Optional<Visit>> getVisitByStoreAndDate(Store store, LocalDate localDate){
       return getVisit(store, localDate)
               .map(visitDbOptional -> convertOptionalVisit(visitDbOptional));
    }

    @Override
    public Completable saveVisit(Visit visit, boolean shouldSendToServer) {
        VisitDb visitDb = visitMapper.applyDb(visit);
       if (!shouldSendToServer){
           return visitDbSource.addItem(visitDb);
       }else{
           return visitDbSource.addItem(visitMapper.applyDb(visit))
                   .andThen(visitNetworkSource.updateItem(visitMapper.applyNetwork(visitDb)));
       }
    }

    @Override
    public Completable removeVisit(Visit visit) {
        return visitDbSource.removeItem(visitMapper.applyDb(visit));
    }

    @Override
    public Single<List<Visit>> getNotSentVisits() {
        VisitSpecification specification = new VisitSpecification();
        RawQuery rawQuery = RawQuery.builder()
                .query(specification.byIsRegistredAndShouldBeSent(String.valueOf(true), String.valueOf(false)).toSqlQuery())
                .build();
        return visitDbSource.getItems(rawQuery)
                .flattenAsObservable(visits -> visits)
                .map(visitDb -> visitMapper.apply(visitDb))
                .toList();
    }

    private Single<Optional<VisitDb>> getVisit(Store store, LocalDate localDate){
        String serverDate = DateUtils.getDateInServerFormatStr(localDate);
        VisitSpecification specification = new VisitSpecification();
        RawQuery rawQuery = RawQuery.builder()
                .query(specification.byStoreIdAndDate(String.valueOf(store.getId()), serverDate).toSqlQuery())
                .build();
        return visitDbSource.getItem(rawQuery);
    }

    private Optional<Visit> convertOptionalVisit(Optional<VisitDb> visitDb){
        if (visitDb.isEmpty()){
            return new Optional<>(null);
        }else{
            return new Optional<>(visitMapper.apply(visitDb.get()));
        }
    }
}
