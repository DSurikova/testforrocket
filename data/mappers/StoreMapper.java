package ru.trmedia.trbtlservice.data.mappers;

import javax.inject.Inject;

import ru.trmedia.trbtlservice.data.models.stores.StoreDb;
import ru.trmedia.trbtlservice.data.network.models.StoreNetwork;
import ru.trmedia.trbtlservice.domain.models.Store;

public class StoreMapper {
    @Inject
    public StoreMapper(){
    }

    public Store apply(StoreDb storeDb){
        Store store = new Store();
        store.setAddress(storeDb.getAddress());
        store.setExternalStoreName(storeDb.getExternalStoreName());
        store.setLatitude(storeDb.getLatitude());
        store.setLongitude(storeDb.getLongitude());
        store.setStoreName(storeDb.getStoreName());
        store.setChannelCode(storeDb.getChannelCode());
        store.setCode(storeDb.getCode());
        store.setId(storeDb.getId());
        store.setCustomerCode(storeDb.getCustomerCode());
        store.setCustomerName(storeDb.getCustomerName());
        store.setChannelName(storeDb.getChannelName());
        store.setSegment(storeDb.getSegment());
        return store;
    }

    public StoreDb applyDb(Store store){
        StoreDb storeDb = new StoreDb();
        store.setAddress(storeDb.getAddress());
        store.setExternalStoreName(storeDb.getExternalStoreName());
        store.setLatitude(storeDb.getLatitude());
        store.setLongitude(storeDb.getLongitude());
        store.setStoreName(storeDb.getStoreName());
        store.setChannelCode(storeDb.getChannelCode());
        store.setCode(storeDb.getCode());
        store.setId(storeDb.getId());
        store.setCustomerCode(storeDb.getCustomerCode());
        store.setCustomerName(storeDb.getCustomerName());
        store.setChannelName(storeDb.getChannelName());
        store.setSegment(storeDb.getSegment());
        return storeDb;
    }

    public StoreDb applyNetwork(StoreNetwork store){
        StoreDb storeDb = new StoreDb();
        store.setAddress(storeDb.getAddress());
        store.setExternalStoreName(storeDb.getExternalStoreName());
        store.setLatitude(storeDb.getLatitude());
        store.setLongitude(storeDb.getLongitude());
        store.setStoreName(storeDb.getStoreName());
        store.setChannelCode(storeDb.getChannelCode());
        store.setCode(storeDb.getCode());
        store.setId(storeDb.getId());
        store.setCustomerCode(storeDb.getCustomerCode());
        store.setCustomerName(storeDb.getCustomerName());
        store.setChannelName(storeDb.getChannelName());
        store.setSegment(storeDb.getSegment());
        return storeDb;
    }
}
