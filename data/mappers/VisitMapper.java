package ru.trmedia.trbtlservice.data.mappers;

import javax.inject.Inject;

import ru.trmedia.trbtlservice.data.models.stores.StoreDb;
import ru.trmedia.trbtlservice.data.models.visit.VisitDb;
import ru.trmedia.trbtlservice.data.network.models.VisitNetwork;
import ru.trmedia.trbtlservice.domain.models.Visit;

public class VisitMapper {
    @Inject
    StoreMapper storeMapper;
    @Inject
    VisitStatusMapper visitStatusMapper;

    @Inject
    public VisitMapper(){
    }

    public Visit apply(VisitDb visitDb, StoreDb storeDb){
       Visit visit = apply(visitDb);
       visit.setStore(storeMapper.apply(storeDb));
       return visit;
    }

    public Visit apply(VisitDb visitDb){
        Visit visit = new Visit();
        visit.setDate(visitDb.getDate());
        visit.setMobileId(visitDb.getMobileId());
        visit.setRegistered(visitDb.isRegistered());
        visit.setShouldBeSent(visitDb.isShouldBeSent());
        visit.setStartTimeByDevice(visitDb.getStartTimeByDevice());
        visit.setStartTimeByGPS(visitDb.getStartTimeByGPS());
        visit.setStartTimeByServer(visitDb.getStartTimeByServer());
        visit.setStoreId(visitDb.getStoreId());
        visit.setTimeSpent(visitDb.getTimeSpent());
        visit.setEnVisitStatus(visitStatusMapper.toDomain(visitDb.getStatusTypeId()));
        return visit;
    }


    public VisitDb applyDb(Visit visit){
        VisitDb visitDb = new VisitDb();
        visitDb.setDate(visit.getDate());
        visitDb.setMobileId(visit.getMobileId());
        visitDb.setRegistered(visit.isRegistered());
        visitDb.setShouldBeSent(visit.isShouldBeSent());
        visitDb.setStartTimeByDevice(visit.getStartTimeByDevice());
        visitDb.setStartTimeByGPS(visit.getStartTimeByGPS());
        visitDb.setStartTimeByServer(visit.getStartTimeByServer());
        visitDb.setTimeSpent(visit.getTimeSpent());
        visitDb.setStoreId(visit.getStoreId());
        visitDb.setStatusTypeId(visitStatusMapper.toData(visit.getEnVisitStatus()));
        return visitDb;
    }

    public VisitNetwork applyNetwork(VisitDb visitDb){
        VisitNetwork visit = new VisitNetwork();
        visitDb.setDate(visitDb.getDate());
        visitDb.setMobileId(visitDb.getMobileId());
        visitDb.setRegistered(visitDb.isRegistered());
        visitDb.setShouldBeSent(visitDb.isShouldBeSent());
        visitDb.setStartTimeByDevice(visitDb.getStartTimeByDevice());
        visitDb.setStartTimeByGPS(visitDb.getStartTimeByGPS());
        visitDb.setStartTimeByServer(visitDb.getStartTimeByServer());
        visitDb.setTimeSpent(visitDb.getTimeSpent());
        visitDb.setStoreId(visitDb.getStoreId());
        visitDb.setStatusTypeId(visitDb.getStatusTypeId());
        return visit;
    }
}
