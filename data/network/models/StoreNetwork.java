package ru.trmedia.trbtlservice.data.network.models;

import com.google.gson.annotations.Expose;

public class StoreNetwork {
    @Expose
    protected int id;
    @Expose
    protected String code;
    @Expose
    protected String storeName;
    @Expose
    protected String externalStoreName;
    @Expose
    protected String segment;
    @Expose
    protected int promoperiod;
    @Expose
    protected String address;
    @Expose
    protected double latitude;
    @Expose
    protected double longitude;
    @Expose
    protected String channelCode;
    @Expose
    protected String channelName;
    @Expose
    protected String customerName;
    @Expose
    protected String customerCode;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getExternalStoreName() {
        return externalStoreName;
    }

    public void setExternalStoreName(String externalStoreName) {
        this.externalStoreName = externalStoreName;
    }

    public String getSegment() {
        return segment;
    }

    public void setSegment(String segment) {
        this.segment = segment;
    }

    public int getPromoperiod() {
        return promoperiod;
    }

    public void setPromoperiod(int promoperiod) {
        this.promoperiod = promoperiod;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }
}
