package ru.trmedia.trbtlservice.data.network.models;


public class VisitNetwork {
	
    protected String mobileId;
    protected int date;
    protected Store store;
    protected int storeId;
    protected long timeSpent;
    protected long startTimeByDevice;
    protected long startTimeByGPS;
    protected long startTimeByServer;
    protected boolean shouldBeSent;
    protected boolean isRegistered;
    protected int statusTypeId;

    public String getMobileId() {
        return mobileId;
    }

    public void setMobileId(String mobileId) {
        this.mobileId = mobileId;
    }

    public int getDate() {
        return date;
    }

    public void setDate(int date) {
        this.date = date;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    public int getStoreId() {
        return storeId;
    }

    public void setStoreId(int storeId) {
        this.storeId = storeId;
    }

    public long getTimeSpent() {
        return timeSpent;
    }

    public void setTimeSpent(long timeSpent) {
        this.timeSpent = timeSpent;
    }

    public long getStartTimeByDevice() {
        return startTimeByDevice;
    }

    public void setStartTimeByDevice(long startTimeByDevice) {
        this.startTimeByDevice = startTimeByDevice;
    }

    public long getStartTimeByGPS() {
        return startTimeByGPS;
    }

    public void setStartTimeByGPS(long startTimeByGPS) {
        this.startTimeByGPS = startTimeByGPS;
    }

    public long getStartTimeByServer() {
        return startTimeByServer;
    }

    public void setStartTimeByServer(long startTimeByServer) {
        this.startTimeByServer = startTimeByServer;
    }

    public boolean isShouldBeSent() {
        return shouldBeSent;
    }

    public void setShouldBeSent(boolean shouldBeSent) {
        this.shouldBeSent = shouldBeSent;
    }

    public boolean isRegistered() {
        return isRegistered;
    }

    public void setRegistered(boolean registered) {
        isRegistered = registered;
    }

    public int getStatusTypeId() {
        return statusTypeId;
    }

    public void setStatusTypeId(int statusTypeId) {
        this.statusTypeId = statusTypeId;
    }
}
