package ru.trmedia.trbtlservice.data.network.sources;

import android.support.annotation.NonNull;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;
import ru.trmedia.trbtlservice.common.PreferencesApi;
import ru.trmedia.trbtlservice.data.network.TRBTLRetrofitInterface;
import ru.trmedia.trbtlservice.data.network.models.StoreNetwork;

public class StoreNetworkSource implements NetworkSource<StoreNetwork> {
    @NonNull
    private final TRBTLRetrofitInterface trbtlRetrofitInterface;
    @NonNull
    private final PreferencesApi preferencesApi;

    public StoreNetworkSource(@NonNull TRBTLRetrofitInterface trbtlRetrofitInterface, @NonNull PreferencesApi preferencesApi) {
        this.trbtlRetrofitInterface = trbtlRetrofitInterface;
        this.preferencesApi = preferencesApi;
    }

    @Override
    public Single<List<StoreNetwork>> getItems() {
        return trbtlRetrofitInterface.getStores(preferencesApi.getJWT());
    }

    @Override
    public Completable updateItem(@NonNull StoreNetwork item) {
        throw new UnsupportedOperationException();
    }
}
