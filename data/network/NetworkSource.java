package ru.trmedia.trbtlservice.data.network.sources;

import android.support.annotation.NonNull;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

public interface NetworkSource<T> {

    Single<List<T>> getItems();

    Completable updateItem(@NonNull T item);
}
