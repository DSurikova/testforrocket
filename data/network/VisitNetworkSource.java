package ru.trmedia.trbtlservice.data.network.sources;

import android.support.annotation.NonNull;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;
import ru.trmedia.trbtlservice.common.PreferencesApi;
import ru.trmedia.trbtlservice.data.network.TRBTLRetrofitInterface;
import ru.trmedia.trbtlservice.data.network.models.VisitNetwork;

public class VisitNetworkSource implements NetworkSource<VisitNetwork>{
    @NonNull
    private final TRBTLRetrofitInterface trbtlRetrofitInterface;
    @NonNull
    private final PreferencesApi preferencesApi;

    public VisitNetworkSource(@NonNull TRBTLRetrofitInterface trbtlRetrofitInterface, @NonNull PreferencesApi preferencesApi) {
        this.trbtlRetrofitInterface = trbtlRetrofitInterface;
        this.preferencesApi = preferencesApi;
    }

    @Override
    public Single<List<VisitNetwork>> getItems() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Completable updateItem(@NonNull VisitNetwork item) {
        return trbtlRetrofitInterface.sendVisit(preferencesApi.getJWT(), item);
    }
}
